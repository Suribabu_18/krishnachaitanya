FROM tomcat:latest
LABEL name="krishnachaitanya@gmail.com"
ADD SampleWebApp.war /usr/local/tomcat/webapps
EXPOSE 8080
CMD ["/opt/tomcat/bin/catalina.sh", "run"]